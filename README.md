Linuxonification
================

[Data sonification](https://en.wikipedia.org/wiki/Data_sonification) is about
making sounds to represent data.  In the case of Linuxonification, it's about
various kinds of data taken from the [Linux kernel](https://kernel.org/).

## Gitophone

One kind of data being considered is the
[history](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/log/)
of changes in [Git](https://git-scm.com/) with all the contributions merged
during a release cycle.  The first kernel version to have its history sonified
here is v5.17.  Although earlier ones can be retroactively sonified too, the
idea is to keep evolving algorithms with each release.  So you're free to run
[`gitophone.py`](gitophone.py) with any part of the kernel history, or even any
Git repository, but the sounds released as part of [this
project](https://soundcloud.com/verdigrix/sets/linuxonification) will only be
tailored ones following each kernel release starting with v5.17.

### How to run it?

First, you'll need a local copy of the Linux kernel source tree from its Git
repository.  For example, to get the mainline one from Linus Torvalds:

```
git clone https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
```

Next, you'll need a few required dependencies.  These are basically
[`pygit2`](https://pypi.org/project/pygit2/) for accessing the Git history and
[`Splat`](https://gitlab.com/verdigris.org/splat) which is another
[verdigris.org](https://verdigris.org/) project for generating the audio.  A
simple way to do this is with Docker to avoid having to install the
dependencies directly on your host system.  Here's a sample command, assuming
the Linux kernel repository is in a local `linux` sub-directory:

```
docker run \
  -v $PWD:/root \
  verdigrix/gitophone \
  /usr/bin/env python3 gitophone.py \
  --start=v5.17 \
  --end=v5.16 \
  --limit=v4.20 \
  --tail=tail-5.16.json \
  --verbose \
  /root/linux
```

If things go as expected, the output should start like this:

```
Repository: /root/linux
  repo:  /root/linux/.git/
  start: f443e374ae131c168a065ea1748feac6b2e76613
  end:   df0cc57e057f18e44dac8e6c18aba47ab53202f9
  limit: 8fe28cb58bcb235034b64cbbb7550a8a43fd88be
Creating tail...
  saving as tail-5.16.json
  tail:  378557 commits
Creating dataset...
  dataset: 14200 commits
  root: f443e374ae13 Linux 5.17
Computing segments...
  segments: 1117
Generating timeline...
Generating audio...
```

With `--verbose` enabled on the command line, some details will be printed to
keep track of the progress.  Eventually, an `output.wav` file should be
produced with the generated audio.

### What are the command line arguments for?

Here's some very basic documentation about how it works:

* `--start`: start revision, which is the last one in the series.  This needs
  to be the most recent one as the algorithm walks backwards through the tree
  of commits to determine incoming merges

* `--end`: end revision, which is the oldest one where the algorithm should
  stop.

* `--limit`: earliest commit when walking through long incoming branches.  This
  is mainly to save processing time by specifying a kernel version that is
  known to be at least older than the oldest commit in the current dataset.

* `--tail`: path to a JSON file to save the "tail" data and avoid computing it
  every time.  The tail data includes all the commits that are should not be in
  the dataset.  This is used to determine when to stop walking though the tree
  of commits.  The tail should contain all commits merged into the master
  branch between the revisions specified by `--limit` and `--end`.

### How is Git history being sonified?

This early version has relatively simple rules for converting the history into
sounds.  Each linear segment with no merge commits uses the same frequency.
The duration of each sound is a logarithmic value taken from the time
difference between two commits on a segment (using the authors' timestamps
which comes from the emails, not when the commit was applied on a branch).
Each segment ends with a merge commit, this is how they are positioned in time
relatively to the "upstream" segment which also contains that merge commit.
The master branch is the first and main segment from which all the others are
derived via merge commits.  An initial frequency is assigned to the main
segment (440Hz which is a middle A note), then each sub-segment gets assigned a
new frequency by multiplying it by a ratio picked randomly from a fixed set.
This creates "just intonation" intervals.  As this is done recursively with
branches getting merged into other branches, segments that are further away
from the main segment tend to have a more remote frequency (either very high or
very low).  Likewise, distant intervals will start introducing dissonent
frequencies as just intonation does not follow the chromatic scale (a.k.a
micro-tonal music).  The levels are also set for each segment to be positioned
further away from the central point in the stereophonic landscape following the
number of recursion levels while going through incoming segments.  The
left/right choices are made randomly.  Finally, all the sounds are generated
with a simple triangle wave and their duration is half of the time assigned to
each commit.

As a result, the frequency of the last note (e.g. the v5.17 release tag) is
always 440Hz since this is the root commit from the main segment.  Everything
else will vary depending on the history with a deterministic relationship from
a timing point of view.  Due to a few random elements, the generated audio will
however be slightly different every time a same command is run.  The timeline
will always be the same, but the frequencies and stereo levels will be using
different combinations.  This property can be used to generate several
sonifications and pick the most convincing one, for example by avoiding
unfortunate cases where too many segments have the same frequency etc.
